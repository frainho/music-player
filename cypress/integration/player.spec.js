describe('player feature', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.get('.search>input').type('Elvis')
    cy.get('button.search-cta').click()
  })

  it('should be able to select a song, play it, and be able to skip to the next one', () => {
    let text
    cy.get('.info:first').click()
    cy.url().should('include', 'play')
    cy.get('audio')
    cy.get('.details>.name').then($name => {
      text = $name.text()
    })
    cy.get('.controls>.next').click()
    cy.get('.details>.name').then($name => {
      expect($name.text()).not.to.eq(text)
    })
  })
})

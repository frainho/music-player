describe('search feature', () => {
  it('searches for a given artist', () => {
    cy.visit('/')

    cy.get('.search>input').type('Elvis')

    cy.get('ul.song-list').should('have.length', 1)

    cy.get('button.search-cta').click()

    cy.get('ul.song-list').should('be.visible')
  })
})

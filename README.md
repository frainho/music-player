# Music Player

## Analysis

- Needs to be a SPA, with navigation between two pages
  - Angular, Vue or React+Redux
- 1st Page - Search/Results Page
  - Search bar (acepts artists/songs/albums/genres)
  - List the results on the same page with song title and artist
  - More detail section with: album title, release date, cover thumbnail, song length, genre and price
  - Sortable based on song length, genre and price
- 2nd page Detail/Player Page
  - each result of the first page should direct here
  - Should contain a similar design with with current players (Assumption, spotify?)
  - Should display cover detail, basic information about the song
  - Should have basic controls to play, pause
  - Should be able to skip to the next of previous song in the list of search results
  - Should allow you to share what you are listening in our favorite social networks (Insta, Face, Twitter?)
- Going from the second page back to the first, should allow you to see the previous search results
- **Tech Requirements:**
  - Dockerfile
  - Tests
  - Readme with instructions

## Task List

- [x] Create boilerplate
- [x] Add Redux
- [x] Add React Router
- [x] Dockerize aplication for dev env
- [x] Dockerize aplication for pro env
- [x] Setup Cypress Tests
- [x] Create Search Component
- [x] Create search service
- [x] Use search service in app component
- [x] Create List of results
- [x] Add more detail section
- [x] Add sorting
- [x] Create Player page
- [x] Add basic player controls (play/pause)
- [x] Skip to the next or previous songs in the list of search results
- [ ] Share what you are listening
- [x] Go back and see the same results as before
- [x] Review instructions

## Instructions

### Without docker
- Development: `npm start`
- Build: `npm run build`

### With docker
- Build Dev image: `npm run build-dev-docker`
- Run Dev: `run-dev-docker`
- Build Pro image: `docker build -t music-player:pro .`
- Run Pro: `docker run -p 80:80 --rm music-player:pro`

### Tests

Run tests with `npm test` for unit tests 
And `npm run test:integration` for journey tests. These tests required the aplication to be running in localhost:3000


## Decisions, Assumptions, Feelings

- Added two dockerfiles, one for dev and another for pro. 
I'm not aware of how common this is but I understand that this way the local environment of the app is closer to the production one
- Used React and Redux.
Took assignment also as an oportunity to explore React and Redux. I was able to experiment a bit with the hooks of Redux but it still feels that the use of redux across the aplication is a bit inconsistent.
- TDD
Initially I tried a TDD like approach but I was lacking a better understanding of react to know how to build the tests first. I defaulted to do the tests after but there are parts of the aplication that are still untested.
- Folder structure
Feeling a bit insecure about the folder structure, would be nice to have some feedback regarding this
- Some improvements I would do:
Add tests for the least tested parts
Styling on the Player page
I feel that some components are to big when it comes to the `react way` of small components
The search input could be a form to enable Enter key submission
Review the accessibility
For the integration tests, mock the response of the server
import SearchService from '../../services/search'
import HttpClient from '../../services/utils/httpClient'

export const UPDATE_SONGS = 'SAVE_SONGS'
export const SORT_SONGS = 'SORT_SONGS'
export const CURRENT_SONG = 'CURRENT_SONG'

export const receivedSongs = songs => ({
  type: UPDATE_SONGS,
  songs
})

export const sortSongs = by => ({
  type: SORT_SONGS,
  by
})

export const getSong = id => ({
  type: CURRENT_SONG,
  id
})

export const fetchSongs = searchTerm => async dispatch => {
  const searchService = new SearchService(new HttpClient())
  const songs = await searchService.search(searchTerm)
  dispatch(receivedSongs(songs))
}

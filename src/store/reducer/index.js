import { UPDATE_SONGS, SORT_SONGS, CURRENT_SONG } from '../actions'

import sorter from '../../services/sort'

const defaultState = {
  songs: [],
  currentSong: {},
  nextSongId: '',
  prevSongId: ''
}

const songsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_SONGS:
      return {
        ...state,
        songs: action.songs
      }
    case SORT_SONGS:
      return {
        ...state,
        songs: [...state].sort(sorter[action.by])
      }
    case CURRENT_SONG:
      return {
        ...state,
        currentSong: state.songs.find(findSongById(action)),
        nextSongId: getNextSong(state, action).trackId,
        prevSongId: getPrevSong(state, action).trackId
      }
    default:
      return state
  }
}

const findSongById = action => song => song.trackId === Number(action.id)

const findSongByIndex = (state, index) => {
  return state.songs[index]
}

const getSongIndexByStep = (state, action, step) => state.songs.indexOf(state.songs.find(findSongById(action))) + step

const getNextSong = (state, action) => {
  const song = getSongByStep(state, action, 1)
  if (!song) return findSongByIndex(state, 0)
  return song
}

const getPrevSong = (state, action) => {
  const song = getSongByStep(state, action, -1)
  if (!song) return findSongByIndex(state, state.songs.length - 1)
  return song
}

const getSongByStep = (state, action, step) => {
  const index = getSongIndexByStep(state, action, step)
  return findSongByIndex(state, index)
}

export default songsReducer

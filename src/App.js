import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import Player from './pages/Player/Player'
import Search from './pages/Search/Search'

function App () {
  return (
    <Router>
      <Switch>
        <Route path="/play/:id">
          <Player/>
        </Route>
        <Route path="/">
          <Search/>
        </Route>
      </Switch>
    </Router>)
}

export default App

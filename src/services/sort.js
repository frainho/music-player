const sorter = {
  genre: (a, b) => a.primaryGenreName > b.primaryGenreName,
  length: (a, b) => a.trackTimeMillis - b.trackTimeMillis,
  price: (a, b) => a.trackPrice - b.trackPrice
}

export default sorter

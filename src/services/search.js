export default class SearchService {
  constructor (httpClient) {
    this.httpClient = httpClient
    this.baseUrl = 'https://itunes.apple.com/'
  }

  async search (term) {
    const url = `${this.baseUrl}search?term=${term}&entity=song&limit=25`
    const response = await this.httpClient.get(url)
    return response.results
  }
}

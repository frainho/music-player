import React from 'react'
import PropTypes from 'prop-types'

import SongListItem from '../SongListItem/SongListItem'
import './List.css'

function List ({ songs, onSort }) {
  return (<ul className="song-list">
    <li>
        Song Name &mdash; Artist &mdash; Sort By &nbsp;
      <select name="sorter" onChange={(e) => onSort(e.target.value)} className="sorter">
        <option value="">Sort By</option>
        <option value="genre">Genre</option>
        <option value="length">Length</option>
        <option value="price">Price</option>
      </select>
    </li>
    {songs.map(song => <SongListItem key={song.trackId} song={song} />)}
  </ul>)
}

List.propTypes = {
  songs: PropTypes.array,
  onSort: PropTypes.func
}

export default List

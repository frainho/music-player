import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './SongListItem.css'
import SongItemDetail from './SongItemDetail'
import { Link } from 'react-router-dom'

const SongListItem = ({ song }) => {
  const [detailStatus, setDetailStatus] = useState({ open: false })

  const toggleDetail = (e) => {
    e.preventDefault()
    setDetailStatus({
      open: !detailStatus.open
    })
  }

  return (<li className="song">
    <Link className="link" to={`/play/${song.trackId}`}>
      <div className="info">
        <p className="name">{song.trackName} &mdash; {song.artistName}</p>
        <button className="more-detail-toggle" onClick={toggleDetail}>{detailStatus.open ? 'Less detail' : 'More detail'}</button>
      </div>
      {detailStatus.open && <SongItemDetail song={song} />}
    </Link>
  </li>)
}

SongListItem.propTypes = {
  song: PropTypes.object
}

export default SongListItem

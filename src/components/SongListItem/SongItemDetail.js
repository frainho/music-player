import React from 'react'
import PropTypes from 'prop-types'

function SongItemDetail ({ song }) {
  return (<div className="extra">
    <img src={song.artworkUrl100} alt="album-cover" />
    <p className="album">Album - {song.collectionName}</p>
    <p className="releaseDate">Release Date - {song.releaseDate}</p>
    <p className="song-length">Song Length - {song.trackTimeMillis}</p>
    <p className="genre">Genre - {song.primaryGenreName}</p>
    <p className="price">Price - {song.trackPrice}</p>
  </div>)
}

SongItemDetail.propTypes = {
  song: PropTypes.object
}

export default SongItemDetail

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './SearchForm.css'

const SearchForm = ({ onSearch }) => {
  const [searchTerm, setSearchTerm] = useState({ term: '' })

  const updateSearchTerm = (e) => setSearchTerm({
    term: e.target.value
  })

  const doSearch = () => onSearch(searchTerm.term)

  return (
    <div className="search">
      <label htmlFor="search-input">Search</label>
      <input type="text" id="search-input" onChange={updateSearchTerm} value={searchTerm.term}/>
      <button className="search-cta" onClick={doSearch}><span role="img" aria-label="magnifying-glass">&#128270;</span></button>
    </div>
  )
}

SearchForm.propTypes = {
  onSearch: PropTypes.func.isRequired
}

export default SearchForm

import React from 'react'
import { useParams, Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import { getSong } from '../../store/actions'

function Player () {
  const currentSong = useSelector(state => state.currentSong)
  const nextSongId = useSelector(state => state.nextSongId)
  const prevSongId = useSelector(state => state.prevSongId)
  const dispatch = useDispatch()
  dispatch(getSong(useParams().id))
  return (
    <main>
      <h1>Music Player</h1>
      <section className="controls">
        <img alt={`artwork-${currentSong.collectionName}`} src={currentSong.artworkUrl100}/>
        <audio controls src={currentSong.previewUrl}/>
        <Link className="prev" to={`/play/${prevSongId}`}>Previous Song</Link>
        <Link className="next" to={`/play/${nextSongId}`}>Next Song</Link>
      </section>
      <section className="details">
        <p className="name">{currentSong.trackName}</p>
        <p className="album">{currentSong.collectionName}</p>
        <p className="genre">{currentSong.primaryGenreName}</p>
        <p className="price">{currentSong.trackPrice}</p>
      </section>
    </main>
  )
}

export default Player

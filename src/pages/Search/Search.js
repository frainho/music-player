import React from 'react'
import PropTypes from 'prop-types'
import SearchForm from '../../components/SearchForm/SearchForm'
import List from '../../components/List/List'

import { fetchSongs, sortSongs } from '../../store/actions'
import { connect } from 'react-redux'

import './Search.css'

function Search ({ songs, sortSongs, searchSongs }) {
  return (
    <>
      <SearchForm onSearch={(term) => searchSongs(term)}/>
      <List onSort={(by) => sortSongs(by)} songs={songs}/>
    </>
  )
}

Search.propTypes = {
  searchSongs: PropTypes.func.isRequired,
  sortSongs: PropTypes.func.isRequired,
  songs: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
  songs: state.songs
})

const mapDispatchToProps = dispatch => ({
  searchSongs: term => {
    dispatch(fetchSongs(term))
  },
  sortSongs: by => {
    dispatch(sortSongs(by))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Search)

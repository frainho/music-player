import { fireEvent, render } from '@testing-library/react'
import { createMemoryHistory } from 'history'
import React from 'react'
import { Router } from 'react-router-dom'
import List from '../../components/List/List'
import Fixtures from '../fixtures/fixtures'

describe('List component', () => {
  let ListComponent
  beforeEach(() => {
    ListComponent = render(
      <Router history={createMemoryHistory()}>
        <List songs={Fixtures.results} onSort={() => { }} />
      </Router>)
  })

  it('renders correctly', () => {
    expect(ListComponent).toMatchSnapshot()
  })

  it('should display the sorting option selected', () => {
    const { getByText } = ListComponent

    fireEvent.change(getByText('Sort By'), { target: { name: 'sorter', value: 'genre' } })

    expect(getByText(/Genre/i)).toBeTruthy()
  })
})

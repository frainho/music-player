import { fireEvent, render } from '@testing-library/react'
import { createMemoryHistory } from 'history'
import React from 'react'
import { Router } from 'react-router-dom'
import SongListItem from '../../components/SongListItem/SongListItem'
import Fixtures from '../fixtures/fixtures'

describe('SongListItem component', () => {
  let SongListItemComponent
  beforeEach(() => {
    SongListItemComponent = render(
      <Router history={createMemoryHistory()}>
        <SongListItem song={Fixtures.results[0]} />
      </Router>
    )
  })

  it('renders correctly', () => {
    expect(SongListItemComponent).toMatchSnapshot()
  })

  it('should show detail section when clicking the more detail button', () => {
    const { queryByText, getByText, getAllByText } = SongListItemComponent

    expect(queryByText(/^(Album|Release Date|Song Length|Genre|Price)/i)).toBeFalsy()

    fireEvent.click(getByText(/More Detail/i))

    expect(getAllByText(/^(Album|Release Date|Song Length|Genre|Price)/i)).toBeTruthy()

    expect(getByText(/Less Detail/i)).toBeTruthy()
  })
})

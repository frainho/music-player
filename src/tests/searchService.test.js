import FakeHttpClient from './mocks/FakeHttpClient'
import SearchService from '../services/search'
import Fixtures from './fixtures/fixtures'

describe('the search client', () => {
  const httpClientMock = new FakeHttpClient()

  it('searches for a given artist', async () => {
    const searchService = new SearchService(httpClientMock)
    const searchResult = await searchService.search('Michael')
    expect(searchResult).toEqual(Fixtures.results)
  })
})

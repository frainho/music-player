import { render } from '@testing-library/react'
import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import Search from '../../pages/Search/Search'
import Fixtures from '../fixtures/fixtures'
import reducer from '../../store/reducer'

describe('Search page component', () => {
  let SearchPageComponent

  beforeEach(() => {
    SearchPageComponent = renderWithRedux(
      <Search songs={Fixtures.results} sortSongs={() => {}} searchSongs={() => {}} />
    )
  })

  it('renders correctly', () => {
    expect(SearchPageComponent).toMatchSnapshot()
  })
})

function renderWithRedux (ui, { store = createStore(reducer) } = {}) {
  return render(<Provider store={store}>{ui}</Provider>)
}
